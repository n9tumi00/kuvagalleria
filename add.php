
<?php
require_once 'inc/top.php';
?>

<h3>Lisää kuva</h3>

<form action="save.php" method="post" enctype="multipart/form-data">
    <div>
        <label for="file">Tiedosto</label>
        <input type="file" class="form-control" name="file" id="file">
        <button class="btn btn-primary">Lataa</button>
    </div>
</form>
<?php
require_once 'inc/bottom.php';
?>